import static org.junit.jupiter.api.Assertions.*;
    import org.junit.jupiter.api.Test;

public class TestFibonacci {
    @Test
    void testGetFirstNum(){
        Fibonacci f = new Fibonacci(1, 2);
        assertEquals(1, f.getFirstNum());
        //test the gets
    }
    @Test
    void testGetSecondNum(){
        Fibonacci f = new Fibonacci(1, 2);
        assertEquals(2, f.getSecondNum());
    }
    @Test
    void testCalculations(){
        Fibonacci f = new Fibonacci(1, 2);
        assertEquals(3, f.calculations());
    }
    @Test
    void testGetTerm(){
        Fibonacci f = new Fibonacci(1, 2);
        assertEquals(1, f.getTerm(1));
    }

    @Test
    void negativeNum(){
        Fibonacci f = new Fibonacci(-2, -3);
        assertEquals(-5, f.calculations());
    }
}
