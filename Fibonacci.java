public class Fibonacci extends Sequence{
    private int firstNum;
    private int secondNum;

    public Fibonacci(int firstNum, int secondNum){
        this.firstNum = firstNum;
        this.secondNum = secondNum;
    }

    public int calculations(){
        return 0;
    }

    public int getFirstNum(){
        return this.firstNum;
    }
    public int getSecondNum(){
        return this.secondNum;
    }

    public int getTerm(int n){
        int nth = 0;
        for(int i = 0 ; i <= n ; i++){
            nth = this.firstNum + this.secondNum;
            this.firstNum = this.secondNum;
            this.secondNum = nth;
            System.out.println(nth);
        }
        return nth;
    }
}