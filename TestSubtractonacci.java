import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestSubtractonacci {
    

    @Test
    public void calculation(){
        Subtractonacci a = new Subtractonacci(1, 2);
        assertEquals(-4, a.getTerm(2));
    }

    @Test
    public void calculationsNegatives(){
        Subtractonacci a = new Subtractonacci(-1, -2);
        assertEquals(4, a.getTerm(2));
    }

    @Test 
    public void testGetTerm(){
        Subtractonacci a = new Subtractonacci(-1, 2);
        assertEquals(0, a.getTerm(-1));
    }
}