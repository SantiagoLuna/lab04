import java.util.Scanner;

public class Application {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        //print method here

        String s = in.next();
        print(parse(s), 10);
        in.close();
    }


    public static void print(Sequence[] s, int n){
        for (Sequence b : s){
            if (b instanceof Fibonacci){
                System.out.println("Fibonacci: " + ((Fibonacci)b).getTerm(n-2));
            }
            else if (b instanceof Subtractonacci){
                System.out.println("Subtractonacci: " + ((Subtractonacci)b).getTerm(n-3));
            }
        }
    }

    public static Sequence[] parse(String s){
        int count = 0;
        Sequence[] seq = new Sequence[s.length()]; //do not know what the length should be:(
        String[] value = s.split(";"); 
        
        
        for(int i=0; i<value.length; i+=4){
            System.out.println(i);
            System.out.println(value[i]);
            if(value[i].equals("Fib")){
                seq[count] = new Fibonacci(Integer.parseInt(value[i+1]), Integer.parseInt(value[i+2]));
                count++;
                System.out.println("you called for fibonacci!");
            }
            if(value[i].equals("Sub")){
                seq[count] = new Subtractonacci(Integer.parseInt(value[i+1]), Integer.parseInt(value[i+2]));
                count++;
                System.out.println("you called for subtractonacci!");
            }
        }
        return seq;
    }
}

    